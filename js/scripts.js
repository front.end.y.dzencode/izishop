$(document).ready(function() {
	//скрипт для бургера
	$('.nav-opener').on('click', function(e) {
		$('body').toggleClass('nav-active');
	});

	//сдайдер с примерами цен
	$('.flooring-slider').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1201,
				settings: {
				  	slidesToShow: 2,
				  	arrows: false,
					dots: true
				}
			  },
			{
			  breakpoint: 768,
			  settings: {
				slidesToShow: 1,
				dots: true
			  }
			}
		]
	});

	//слайдер отзывов
	$('.slider-result').slick({
		// slidesToShow: 1,
		// slidesToScroll: 1,
		fade: true,
  		cssEase: 'linear',
		responsive: [
			{
				breakpoint: 1201,
				settings: {
					arrows: false,
					dots: true
				}
			}
		]
	});

	//слайдер Другие продукты
	$('.other-product-slider').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1201,
				settings: {
					slidesToShow: 3,
					arrows: false,
					dots: true
				}
			},
			{
				breakpoint: 1025,
				settings: {
					slidesToShow: 2,
					arrows: false,
					dots: true
				}
			},
			{
				breakpoint: 769,
				settings: {
					slidesToShow: 1,
					arrows: false,
					dots: true
				}
			}
		]
	});

	//добавление поля формы
    $('.add-form-row').click(function(e){
        e.preventDefault();
        var inputRow = $(this).closest('.order-form').find('.send-row').first().clone();
        inputRow.find('input').val('');
        inputRow.appendTo($(this).closest('.order-form').find('.send-wrapper'));
	});
	
	//удаление поля формы
    $('body').on('click', '.remove-link', function(e){
        e.preventDefault();
        $(this).closest('.send-row').remove();
    });

    //очистка поля формы
    $('.clear-fields').click(function(e){
        e.preventDefault();
        $(this).closest('form')[0].reset();
	});
	
	//добавление класса на секцию при скроле
	var animSection = $('.servises-section');
	$(document).on('scroll', function () {
		var position = $(window).scrollTop(),
		block_position = $('.servises-section').offset().top - (animSection.innerHeight() - 100);
		if (position > block_position) {
			animSection.addClass('animate-section');
		} else {
			animSection.removeClass('animate-section');
		}
	});

	//валидация формы
	$('.order-form').validate({
		rules: {
			product: {
				required: true
			},
			quantity: {
				required: true
			},
		},
		messages: {
			product: {
				required: false,
			},
			quantity: {
				required: false,
			}
		}
	});
});